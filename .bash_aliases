alias cs='find . -name "*.h" -o -name "*.c" -o -name "*.cpp" -o -name "*.java" -o -name "*.cc" > cscope.files;cscope -bkq -i cscope.files'

alias sd='svn diff | dos2unix | colordiff | less -s -M +Gg'
alias gd='git diff | dos2unix | colordiff | less -s -M +Gg'

alias svnlog='svn log --xml > ~/svnLog.xml'
alias cdsys='cd ../lichee/tools/pack/chips/sun8iw6p1/configs/'
alias cdsof='cd ../android/device/softwinner'



##编译
alias mm='mm -j12'
alias mmdex='mm -j8 DEX_FLAG:=true'

#-------------------
# 个人的别名
#-------------------

#alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
# -> 防止偶然的文件误操作. 
alias mkdir='mkdir -p'

alias h='history'
alias j='jobs -l'
alias r='rlogin'
alias which='type -all'
alias ..='cd ..'
alias path='echo -e ${PATH//:/\\n}'
alias print='/usr/bin/lp -o nobanner -d $LPDEST'   # 假设LPDEST被定义
alias pjet='enscript -h -G -fCourier9 -d $LPDEST'  # 使用enscript的漂亮的打印
alias background='xv -root -quit -max -rmode 5'    # 将一张图片作为背景
alias du='du -kh'
alias df='df -kTh'

# 'ls'家族 (假定使用GNU ls)
alias la='ls -Al'               # 显示隐藏文件
alias ls='ls -hF --color'	# 为识别的文件类型添加颜色
alias lx='ls -lXB'              # 按扩展名排序
alias lk='ls -lSr'              # 按尺寸排序
alias lc='ls -lcr'		# 按修改时间排序
alias lu='ls -lur'		# 按访问时间排序
alias lr='ls -lR'               # 递归ls
alias lt='ls -ltr'              # 按日期排序
alias lm='ls -al |more'         # 管道给'more'
alias tree='tree -Csu'		# 'ls'的另一种好方法

# 裁减'less'
alias more='less'
export PAGER=less
export LESSCHARSET='latin1'
export LESSOPEN='|/usr/bin/lesspipe.sh %s 2>&-' # 如果lesspipe.sh存在, 就用这个
export LESS='-i -N -w  -z-4 -g -e -M -X -F -R -P%t?f%f \
:stdin .?pb%pb\%:?lbLine %lb:?bbByte %bb:-...'

alias grep='grep --color=auto --exclude-dir={.git,.svn}'
